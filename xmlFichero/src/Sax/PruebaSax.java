package Sax;

import org.xml.sax.Attributes;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
/**
 * Clase que muestra el contenido de una fichero XML usando la API SAX, usando una clase GestorContenido que usa la interfaz DefaultHandler.
 * Tambi�n permite validaci�n usando una clase que usa la interfaz ErrorHandler.
 * 
 * @author PSGCesur
 * */

public class PruebaSax {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		XMLReader procesadorXML;
		try {
			//Instanciamos las clases de gesti�n y errores
			GestionContenido gestor= new GestionContenido(); 
			MyErrorHandler error = new MyErrorHandler();
			
			
			System.out.println("***********************************");
			
			procesadorXML = XMLReaderFactory.createXMLReader();
			//Para validacion
			// procesadorXML.setFeature("http://xml.org/sax/features/validation", true);
			 procesadorXML.setErrorHandler(error);
			
			//Para que mostrar nuestros propios mensajes
			procesadorXML.setContentHandler(gestor); 
			
			//InputSource fileXML = new InputSource("alumnos.xml"); 
			//Si tiene DTD podemos usar validaci�n
			InputSource fileXML = new InputSource("tarea.xml"); 
			
			//Para que haga su magia
			procesadorXML.parse(fileXML);
			
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
//Gestor del contenido del XML
class GestionContenido extends DefaultHandler {
	    
	public GestionContenido() {
		super(); 
	}

	public void startDocument() throws SAXException { 
		System.out.println("***Inicio XML***");
	}

	public void endDocument() { 
		System.out.println("\n***Final XML***" );
	}
/*
	//lectura de etiqueta de apertura que puede contener atributos
	  public void startElement(String namespaceURI, String localName, 
	                           String qName, Attributes atts) 
	                           throws SAXException {
	     System.out.println("startElement: " + qName);
	     System.out.println(namespaceURI);
	     System.out.println(localName);
	     // Lista atributos y sus valores
	     for (int i=0; i<atts.getLength(); i++) {
	        System.out.println("Atributo: " + atts.getLocalName(i));
	        System.out.println("\tValor: " + atts.getValue(i));
	     }
	  }
	*/
	public void startElement(String uri, String nombre, 
			String nombreC, Attributes atts) { 
		System.out.print("\n  <" + nombre +"> "); 
	} 
//*/
	public void endElement(String uri, String nombre, String nombreC){ 
		if (nombre== "cine") {
			System.out.print("\n");
		}
		if (nombre == "pelicula") {
			System.out.print("\n");
		}
		System.out.print(" </"+nombre +">  " );
	}
	
	///*
	//lectura de caracteres
	  public void characters(char[] ch, int start, int length)
	                         throws SAXException {
	     String charString = new String(ch, start, length); 
	     System.out.print(charString); 
	  }
//*/
	  /*
	   //lectura de caracteres sin saltos de l�nea.
	public void characters(char[] ch, int inicio, int longitud) throws SAXException { 
		String car=new String(ch, inicio, longitud); 
		car = car.replaceAll("[\t\n]","");//quitar saltos de linea 
		if(!car.isEmpty())System.out.println ("\tCaracteres: " + car); 
	}
	*/
	
	//espacio en blanco que se puede ignorar
	/*  public void ignorableWhitespace(char[] ch, int start, int length) 
	                                  throws SAXException {
	     System.out.println(length + " caracteres en blanco ignorables"); 
	  }
	  */
	
	  
} //fin GestionContenido 

class MyErrorHandler implements ErrorHandler {


    private String getParseExceptionInfo(SAXParseException spe) {
        String systemId = spe.getSystemId();

        if (systemId == null) {
            systemId = "null";
        }

        String info = "URI=" + systemId + " Line=" 
            + spe.getLineNumber() + ": " + spe.getMessage();

        return info;
    }

    public void warning(SAXParseException spe) throws SAXException {
    	System.out.println("Warning: " + getParseExceptionInfo(spe));
    }
        
    public void error(SAXParseException spe) throws SAXException {
        String message = "Error: " + getParseExceptionInfo(spe);
        throw new SAXException(message);
    }

    public void fatalError(SAXParseException spe) throws SAXException {
        String message = "Fatal Error: " + getParseExceptionInfo(spe);
        throw new SAXException(message);
    }
}