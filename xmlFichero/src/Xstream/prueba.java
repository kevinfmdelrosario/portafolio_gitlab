package Xstream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class prueba {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Pelicula l1 = new Pelicula ("pelicula1", "Maria", "10/01/1999", 1, "comedia");
	Pelicula l2 = new Pelicula ("pelicula2", "Jose", "12/11/1899", 1, "ficcion");

	Cine cn1 = new Cine ();
	cn1.addPelicula(l1);
	cn1.addPelicula(l2);
	
	cn1.info();
	System.out.println("*******");
	
	//forma 1 para mostrar el xml
	XStream x1 = new XStream(new DomDriver());
	//String xml = x1.toXML(cn1);
	//System.out.println(xml);
	
	
	//forma2
	x1.alias("Cine",Cine.class);		// para cambiar el nombre y poner un alias
	x1.alias("pelicula",Pelicula.class); 
	x1.omitField(Pelicula.class, "id"); // para omitir un campo
	
	x1.useAttributeFor(Pelicula.class, "autor"); // lo vuelve un atributo ( <pelicula autor="Maria">)
	x1.aliasField("campEstreno", Pelicula.class, "estreno"); // cambia el nombre de los campos
	String xml = x1.toXML(cn1);
	
	System.out.println(xml); // muestra el xml
	
	// para volverlo al final xml
	try {
		x1.toXML(cn1, Files.newBufferedWriter(Paths.get("Cine.xml"))); // NOMBRE DE COMO SE LLAMARA
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	//Busca un 
	try {
		Cine cn2 = (Cine) x1.fromXML(Files.newBufferedReader(Paths.get("Cine.xml")));
		cn2.info();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	
	}

}
