package Xstream;

public class Pelicula {

	private String nombre;
	private String autor;
	private String estreno;
	private int id;
	private String genero;
	

	public Pelicula(String nombre, String autor, String estreno, int id, String genero) {
		super();
		this.nombre = nombre;
		this.autor = autor;
		this.estreno = estreno;
		this.id = id;
		this.genero = genero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getEstreno() {
		return estreno;
	}

	public void setEstreno(String estreno) {
		this.estreno = estreno;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Pelicula"
				+ "\nNombre: "+ nombre
				+"\nAutor: "+ autor
				+"\nEstreno: "+ estreno
				+"\nId: " + id
				+"\nGenero: " + genero;
	}
}
