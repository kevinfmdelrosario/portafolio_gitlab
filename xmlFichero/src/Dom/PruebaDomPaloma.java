package Dom;

import java.io.File;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class PruebaDomPaloma {

	public static void main(String[] args) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		String fichero = "alumnos.xml";
		
		try {
			
			DocumentBuilder builder = dbf.newDocumentBuilder();
			Document doc = builder.parse(new File(fichero));
			
			//obtener el nodo ra�z
			System.out.println("Elemento ra�z:" + doc.getDocumentElement().getNodeName());
			
			// listar todo el documento
			NodeList alumnos = doc.getElementsByTagName("alumno");
			
			//recorrer la lista de nodos alumno
			for (int i = 0; i < alumnos.getLength(); i++) {
				Node alumno = alumnos.item(i);
				System.out.println("->Alumno");
				if(alumno.getNodeType() == Node.ELEMENT_NODE) {
					Element elemento = (Element) alumno;
					
					// ----- Obtener los valores de nombre de cada alumno
					NodeList nodo = elemento.getElementsByTagName("nombre").item(0).getChildNodes();
					Node valornodo = (Node) nodo.item(0);
					
					System.out.println("\t -Nombre: " + valornodo.getNodeValue());
					
					// -----
					NodeList nodoAp = elemento.getElementsByTagName("apellido").item(0).getChildNodes();
					Node valornodoAp = (Node) nodoAp.item(0);
					
					System.out.println("\t -Apellido: " + valornodoAp.getNodeValue());
				}
			}
			
			//pedir datos
			Scanner sc = new Scanner(System.in);
			System.out.println("Inserte nombre de alumno:");
			String nombre = sc.nextLine();
			System.out.println("Inserte apellido de alumno:");
			String apellido = sc.nextLine();
			/*System.out.println("Inserte clase de alumno:");
			String clase = sc.nextLine();
			System.out.println("Inserte fechanac de alumno:");
			String fechanac = sc.nextLine();*/
			
			//crear un elemento alumno
			Element raizAlumno = doc.createElement("alumno");
			doc.getDocumentElement().appendChild(raizAlumno);
			
			//crear un elemento nombre
			Element nombreElement = doc.createElement("nombre");
			Text nombreTexto = doc.createTextNode(nombre);
			raizAlumno.appendChild(nombreElement);
			nombreElement.appendChild(nombreTexto);
			
			//crear un elemento apellido
			Element apellidoElement = doc.createElement("apellido");
			Text apellidoTexto = doc.createTextNode(apellido);
			raizAlumno.appendChild(apellidoElement);
			apellidoElement.appendChild(apellidoTexto);
			
			//para escribir lo que ya se ha hecho
			Source source = new DOMSource(doc);
			Result result = new StreamResult("alumnosadd.xml");
			Transformer trans = TransformerFactory.newInstance().newTransformer();
			trans.transform(source, result);
			
			//borrar
			System.out.println("Inserta el nombre del alumno que quieres borrar");
			String nombreBorrar = sc.nextLine();
			
			NodeList alumnos2 = doc.getElementsByTagName("alumno");
			
			//recorrer la lista de nodos alumno2
			for (int i = 0; i < alumnos2.getLength(); i++) {
				Node alumno2 = alumnos2.item(i);
				if(alumno2.getNodeType() == Node.ELEMENT_NODE) {
					Element elemento = (Element) alumno2;
					NodeList nodo = elemento.getElementsByTagName("nombre").item(0).getChildNodes();
					Node valornodo = (Node) nodo.item(0);
					
					if(valornodo.getNodeValue().equals(nombreBorrar)) {
						alumno2.getParentNode().removeChild(alumno2);
					}
				}
				
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
