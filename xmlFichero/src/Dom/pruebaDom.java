package Dom;

import java.io.File;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.w3c.dom.*;

public class pruebaDom {
	private static final DocumentBuilderFactory TransformerFactory = null;

	public static void main(String[] args) {
		
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newNSInstance();		//se crea el builder, 
		String fichero = "alumnos.xml";
		
		try {
			
			DocumentBuilder builder = dbf.newDocumentBuilder();
			Document doc = builder.parse( new File(fichero));		// importamos: import org.w3c.dom.Document;
			
			//obtener el nodo raiz
			System.out.println("Elemento raiz: " + doc.getDocumentElement().getNodeName()); // nos muestra la raiz de todo el xml (nodo raiz)
			
			
			//Listar todo el documento
			NodeList alumnos = doc.getElementsByTagName("alumno");   //listamos todo y vamos a recorrer con el for
			
			
			//recorrer todo el documento
			for (int i = 0; i < alumnos.getLength(); i++) {		//obtener la longitud con el getLength
				Node alumno = alumnos.item(i);					//item posicicionarnos
				//nodo es la madre que tenga raiz (atributos con mas atributos)
				
				System.out.println("--> alumno");
				
				if(alumno.getNodeType()== Node.ELEMENT_NODE) {
					Element elemento = (Element) alumno;
					
					//---- Muestra nombre
					
					NodeList nodo = elemento.getElementsByTagName("nombre").item(0).getChildNodes();
					Node valornodo = (Node) nodo.item(0);
					
					System.out.println("\t -Nombre: " + valornodo.getNodeValue());
					
					//-- Mostrar apellido
					
					NodeList nodoAp = elemento.getElementsByTagName("apellido").item(0).getChildNodes();
					Node valornodoAp = (Node) nodoAp.item(0);
					
					System.out.println("\t -Apellido: " + valornodoAp.getNodeValue());
					
				}
				
			}
			
			
			// pedir datos 
			
			Scanner sc = new Scanner(System.in);
			//nombre
			System.out.println("inserte nombre");
			String nombre = sc.nextLine();
			//apellido
			System.out.println("inserte apellido");
			String apellido = sc.nextLine();
			//clase
			System.out.println("inserte clase");
			String clase = sc.nextLine();
			//fecha nacimiento
			System.out.println("inserte fecha Nacimiento");
			String fechanac = sc.nextLine();
			
			
			//crear un elemento alumno
			Element raizAlumno = doc.createElement("alumno");
			doc.getDocumentElement().appendChild(raizAlumno);
			
			//crear un elemento nombre
			Element nombreElement = doc.createElement("alumno");
			Text nombreTexto = doc.createTextNode(nombre);
			raizAlumno.appendChild(nombreElement);
			nombreElement.appendChild(nombreTexto);
			

			//crear un elemento apellido
			Element apellidoElement = doc.createElement("apellido");
			Text apellidoTexto = doc.createTextNode(apellido);
			raizAlumno.appendChild(apellidoElement);
			apellidoElement.appendChild(apellidoTexto);
			
			
			//guarda los datos
			Source source = new DOMSource(doc);
			Result result = new StreamResult("alumnosadd.xml");
		//	Transformer trans = TransformerFactory.newInstance() .newTransformer();
		//	trans.transform(source, result);
			
			
		
			
			//borrar
			System.out.println("Inserta el nombre del alumno que quieres borrar");
			String nombreBorrar = sc.nextLine();
			
			NodeList alumnos2 = doc.getElementsByTagName("alumno");
			
			
			
				//recorrer la lista de nodos alumno2
			for (int i = 0; i < alumnos2.getLength(); i++) {
				Node alumno2 = alumnos2.item(i);
				if(alumno2.getNodeType() == Node.ELEMENT_NODE) {
					Element elemento = (Element) alumno2;
					NodeList nodo = elemento.getElementsByTagName("nombre").item(0).getChildNodes();
					Node valornodo = (Node) nodo.item(0);
					
					if(valornodo.getNodeValue().equals(nombreBorrar)) {
						alumno2.getParentNode().removeChild(alumno2);
					}
				}
			
		
			}
		} catch (Exception e) {
		}
		
		
		/*
		 * Crear un xml
		 * menu: 
		 * 	-leer xml
		 * 	-insertar
		 * 	-borrar xml
		 * 
		 */
	
	}
}
